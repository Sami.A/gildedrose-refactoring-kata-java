package com.gildedrose.updater;

import com.gildedrose.Item;
import com.gildedrose.builder.ItemBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ImprovesQualityUpdaterTest
{

    private static ImprovesQualityUpdater improvesQualityUpdater;

    @BeforeAll
    static void init()
    {
        improvesQualityUpdater = new ImprovesQualityUpdater();
    }

    @Test
    @DisplayName("SellInValue should decrease for Aged Brie")
    void decreasesSellInValues()
    {
        Item item =
            new ItemBuilder("Aged Brie").toBeSoldIn(10).ofQuality(5).build();


        improvesQualityUpdater.update(item);
        assertEquals(9, item.sellIn);
    }

    @Test
    @DisplayName("The Quality should increase twice as fast when the SellIn value becomes negative - Aged Brie item case")
    void IncreasesQualityTwiceAsFastWhenSellInPassedForAgedBrie()
    {
        Item item = new ItemBuilder("Aged Brie").toBeSoldIn(1).ofQuality(10).build();


        improvesQualityUpdater.update(item);

        assertEquals(0, item.sellIn);
        assertEquals(11, item.quality);

        improvesQualityUpdater.update(item);

        assertEquals(-1, item.sellIn);
        assertEquals(13, item.quality);

        improvesQualityUpdater.update(item);

        assertEquals(-2, item.sellIn);
        assertEquals(15, item.quality);
    }

    @Test
    @DisplayName("The Quality should never go over 50 - Aged Brie case")
    void qualityNeverOver50AgedBrie()
    {
        Item item = new ItemBuilder("Aged Brie").toBeSoldIn(10).ofQuality(49).build();

        improvesQualityUpdater.update(item);

        assertEquals(50, item.quality);

        improvesQualityUpdater.update(item);

        assertEquals(50, item.quality);
    }

    @Test
    @DisplayName("The Aged Brie item should have its Quality value increased with time")
    void agedBrieIncreasesQuality()
    {
        Item item = new ItemBuilder("Aged Brie").toBeSoldIn(10).ofQuality(1).build();

        improvesQualityUpdater.update(item);

        assertEquals(2, item.quality);

        improvesQualityUpdater.update(item);

        assertEquals(3, item.quality);
    }
}
