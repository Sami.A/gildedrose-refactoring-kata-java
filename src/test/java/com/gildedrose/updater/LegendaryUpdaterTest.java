package com.gildedrose.updater;

import com.gildedrose.Item;
import com.gildedrose.builder.ItemBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LegendaryUpdaterTest
{
    private static LegendaryUpdater legendaryUpdater;

    @BeforeAll
    static void init()
    {
        legendaryUpdater = new LegendaryUpdater();
    }

    @Test
    @DisplayName("The Sulfuras item should never be sold")
    void sulfurasNotToSell()
    {
        Item item = new ItemBuilder("Sulfuras, Hand of Ragnaros").toBeSoldIn(10).ofQuality(80).build();

        legendaryUpdater.update(item);

        assertEquals(10, item.sellIn);

        legendaryUpdater.update(item);

        assertEquals(10, item.sellIn);
    }

    @Test
    @DisplayName("The Sulfuras should always keep the same quality value with time")
    void SulfurasAlwaysSameQuality()
    {
        Item item = new ItemBuilder("Sulfuras, Hand of Ragnaros").toBeSoldIn(10).ofQuality(80).build();

        legendaryUpdater.update(item);

        assertEquals(80, item.quality);

        legendaryUpdater.update(item);

        assertEquals(80, item.quality);
    }
}
