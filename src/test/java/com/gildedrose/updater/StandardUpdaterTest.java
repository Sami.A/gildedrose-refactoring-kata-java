package com.gildedrose.updater;

import com.gildedrose.Item;
import com.gildedrose.builder.ItemBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StandardUpdaterTest
{
    private static StandardUpdater standardUpdater;

    @BeforeAll
    static void init()
    {
        standardUpdater = new StandardUpdater();
    }

    @Test
    @DisplayName("The Quality should lower twice as fast when the SellIn value becomes negative - Normal item case")
    void lowersQualityTwiceAsFastWhenSellInPassedForNormalItem()
    {
        Item item = new ItemBuilder("Normal Item").toBeSoldIn(1).ofQuality(10).build();

        standardUpdater.update(item);

        assertEquals(0, item.sellIn);
        assertEquals(9, item.quality);

        standardUpdater.update(item);

        assertEquals(-1, item.sellIn);
        assertEquals(7, item.quality);

        standardUpdater.update(item);

        assertEquals(-2, item.sellIn);
        assertEquals(5, item.quality);
    }

    @Test
    @DisplayName("The Quality should never be negative")
    void qualityNeverNegative()
    {
        Item item = new ItemBuilder("Normal Item").toBeSoldIn(10).ofQuality(1).build();

        standardUpdater.update(item);

        assertEquals(0, item.quality);

        standardUpdater.update(item);

        assertEquals(0, item.quality);
    }
}
