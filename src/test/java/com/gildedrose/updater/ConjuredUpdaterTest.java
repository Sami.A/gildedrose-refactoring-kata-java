package com.gildedrose.updater;

import com.gildedrose.Item;
import com.gildedrose.builder.ItemBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConjuredUpdaterTest
{
    private static ConjuredUpdater conjuredUpdater;

    @BeforeAll
    static void init()
    {
        conjuredUpdater = new ConjuredUpdater();
    }

    @Test
    @DisplayName("The Quality should lower twice as fast for the conjured item")
    void lowersQualityTwiceAsFastWhenSellInPassedForNormalItem()
    {
        Item item = new ItemBuilder("Conjured Mana Cake").toBeSoldIn(10).ofQuality(10).build();

        conjuredUpdater.update(item);

        assertEquals(9, item.sellIn);
        assertEquals(8, item.quality);

        conjuredUpdater.update(item);

        assertEquals(8, item.sellIn);
        assertEquals(6, item.quality);

        conjuredUpdater.update(item);

        assertEquals(7, item.sellIn);
        assertEquals(4, item.quality);
    }

    @Test
    @DisplayName("The Quality of the conjured Item should never be negative")
    void qualityNeverNegative()
    {
        Item item = new ItemBuilder("Conjured Mana Cake").toBeSoldIn(10).ofQuality(2).build();

        conjuredUpdater.update(item);

        assertEquals(0, item.quality);

        conjuredUpdater.update(item);

        assertEquals(0, item.quality);
    }
}
