package com.gildedrose.updater;

import com.gildedrose.Item;
import com.gildedrose.builder.ItemBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BackStagePassesUpdaterTest
{
    private static BackStagePassesUpdater backStagePassesUpdater;

    @BeforeAll
    static void init()
    {
        backStagePassesUpdater = new BackStagePassesUpdater();
    }

    @Test
    @DisplayName("SellInValue should decrease for Aged Brie and BackStage Passes")
    void decreasesSellInValues()
    {
        Item item = new ItemBuilder("Backstage passes to a TAFKAL80ETC concert").toBeSoldIn(15).ofQuality(5).build();

        backStagePassesUpdater.update(item);

        assertEquals(14, item.sellIn);
    }

    @Test
    @DisplayName("The Quality should never go over 50 - Back Stage Passes case")
    void qualityNeverOver50BackStagePasses()
    {
        Item item = new ItemBuilder("Backstage passes to a TAFKAL80ETC concert").toBeSoldIn(3).ofQuality(49).build();
        backStagePassesUpdater.update(item);

        assertEquals(50, item.quality);

        backStagePassesUpdater.update(item);

        assertEquals(50, item.quality);
    }

    @Test
    @DisplayName("The BackStage passes should have its Quality value increased by 1 when more than 10 days left")
    void backStagePassesShouldDecreaseBy1WhenMoreThan10DaysLeft()
    {
        Item item = new ItemBuilder("Backstage passes to a TAFKAL80ETC concert").toBeSoldIn(12).ofQuality(20).build();


        backStagePassesUpdater.update(item);

        assertEquals(11, item.sellIn);
        assertEquals(21, item.quality);

        backStagePassesUpdater.update(item);

        assertEquals(10, item.sellIn);
        assertEquals(22, item.quality);
    }

    @Test
    @DisplayName("The BackStage passes should have its Quality value increased by 2 when between 5 and 10 days left")
    void backStagePassesShouldDecreaseBy2WhenBetween5And10DaysLeft()
    {
        Item item = new ItemBuilder("Backstage passes to a TAFKAL80ETC concert").toBeSoldIn(10).ofQuality(20).build();


        backStagePassesUpdater.update(item);

        assertEquals(9, item.sellIn);
        assertEquals(22, item.quality);

        backStagePassesUpdater.update(item);

        assertEquals(8, item.sellIn);
        assertEquals(24, item.quality);

        backStagePassesUpdater.update(item);

        assertEquals(7, item.sellIn);
        assertEquals(26, item.quality);

        backStagePassesUpdater.update(item);

        assertEquals(6, item.sellIn);
        assertEquals(28, item.quality);

        backStagePassesUpdater.update(item);

        assertEquals(5, item.sellIn);
        assertEquals(30, item.quality);
    }

    @Test
    @DisplayName("The BackStage passes should have its Quality value increased with time by 3 when sellIn is 5 days or less")
    void backStagePassesShouldDecreaseBy3When5DaysOrLessLeft()
    {
        Item item = new ItemBuilder("Backstage passes to a TAFKAL80ETC concert").toBeSoldIn(5).ofQuality(20).build();

        backStagePassesUpdater.update(item);

        assertEquals(4, item.sellIn);
        assertEquals(23, item.quality);

        backStagePassesUpdater.update(item);

        assertEquals(3, item.sellIn);
        assertEquals(26, item.quality);

        backStagePassesUpdater.update(item);

        assertEquals(2, item.sellIn);
        assertEquals(29, item.quality);

        backStagePassesUpdater.update(item);

        assertEquals(1, item.sellIn);
        assertEquals(32, item.quality);

        backStagePassesUpdater.update(item);

        assertEquals(0, item.sellIn);
        assertEquals(35, item.quality);
    }

    @Test
    @DisplayName("The BackStage passes should have its Quality to 0 when the SellIn is negative")
    void backStagePassesQualityAfterNegativeSellIn()
    {
        Item item = new ItemBuilder("Backstage passes to a TAFKAL80ETC concert").toBeSoldIn(1).ofQuality(20).build();

        backStagePassesUpdater.update(item);

        assertEquals(0, item.sellIn);
        assertEquals(23, item.quality);

        backStagePassesUpdater.update(item);

        assertEquals(-1, item.sellIn);
        assertEquals(0, item.quality);

        backStagePassesUpdater.update(item);

        assertEquals(-2, item.sellIn);
        assertEquals(0, item.quality);
    }


}
