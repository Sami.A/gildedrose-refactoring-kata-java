package com.gildedrose.updater.provider;

import com.gildedrose.Item;
import com.gildedrose.builder.ItemBuilder;
import com.gildedrose.updater.*;
import com.gildedrose.updater.contract.ItemUpdater;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class UpdaterProviderTest
{
    @Test
    @DisplayName("Update provider should return the correct values - Standard Updater")
    void providerStandardTest()
    {
        Item item = new ItemBuilder("random").build();

        ItemUpdater itemUpdater = UpdaterProvider.getUpdaterFor(item.name);

        assertTrue(itemUpdater instanceof StandardUpdater);
    }

    @Test
    @DisplayName("Update provider should return the correct values - BackStagePasses Updater")
    void providerBackStageTest()
    {
        Item item = new ItemBuilder("Backstage passes to a TAFKAL80ETC concert").build();

        ItemUpdater itemUpdater = UpdaterProvider.getUpdaterFor(item.name);

        assertTrue(itemUpdater instanceof BackStagePassesUpdater);
    }

    @Test
    @DisplayName("Update provider should return the correct values - ImprovesQuality Updater")
    void providerImprovesQualityTest()
    {
        Item item = new ItemBuilder("Aged Brie").build();

        ItemUpdater itemUpdater = UpdaterProvider.getUpdaterFor(item.name);

        assertTrue(itemUpdater instanceof ImprovesQualityUpdater);
    }

    @Test
    @DisplayName("Update provider should return the correct values - Legendary Updater")
    void providerLegendaryTest()
    {
        Item item = new ItemBuilder("Sulfuras, Hand of Ragnaros").build();

        ItemUpdater itemUpdater = UpdaterProvider.getUpdaterFor(item.name);

        assertTrue(itemUpdater instanceof LegendaryUpdater);
    }

    @Test
    @DisplayName("Update provider should return the correct values - Conjured Updater")
    void providerConjuredTest()
    {
        Item item = new ItemBuilder("Conjured Mana Cake").build();

        ItemUpdater itemUpdater = UpdaterProvider.getUpdaterFor(item.name);

        assertTrue(itemUpdater instanceof ConjuredUpdater);
    }


}
