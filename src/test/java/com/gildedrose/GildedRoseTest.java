package com.gildedrose;

import com.gildedrose.builder.ItemBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest
{

    @Test
    @DisplayName("Quality and SellIn value should decrease when calling updateQuality()")
    void lowersQualityAndSellInValues()
    {
        Item[] items = new Item[]{new ItemBuilder("Normal Item").toBeSoldIn(5).ofQuality(5).build()};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(4, app.items[0].sellIn);
        assertEquals(4, app.items[0].quality);

        app.updateQuality();

        assertEquals(3, app.items[0].sellIn);
        assertEquals(3, app.items[0].quality);
    }

    @Test
    @DisplayName("Update Quality should be called on all items")
    void multipleItems()
    {
        Item[] items = new Item[]{
            new ItemBuilder("Normal Item").toBeSoldIn(5).ofQuality(5).build(),
            new ItemBuilder("Aged Brie").toBeSoldIn(10).ofQuality(10).build(),
            new ItemBuilder("Backstage passes to a TAFKAL80ETC concert").toBeSoldIn(15).ofQuality(15).build(),
            new ItemBuilder("Sulfuras, Hand of Ragnaros").toBeSoldIn(15).ofQuality(80).build(),
            new ItemBuilder("Normal Item").toBeSoldIn(20).ofQuality(20).build(),
        };
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(4, app.items[0].sellIn);
        assertEquals(4, app.items[0].quality);

        assertEquals(9, app.items[1].sellIn);
        assertEquals(11, app.items[1].quality);

        assertEquals(14, app.items[2].sellIn);
        assertEquals(16, app.items[2].quality);

        assertEquals(15, app.items[3].sellIn);
        assertEquals(80, app.items[3].quality);

        assertEquals(19, app.items[4].sellIn);
        assertEquals(19, app.items[4].quality);

        app.updateQuality();

        assertEquals(3, app.items[0].sellIn);
        assertEquals(3, app.items[0].quality);

        assertEquals(8, app.items[1].sellIn);
        assertEquals(12, app.items[1].quality);

        assertEquals(13, app.items[2].sellIn);
        assertEquals(17, app.items[2].quality);

        assertEquals(15, app.items[3].sellIn);
        assertEquals(80, app.items[3].quality);

        assertEquals(18, app.items[4].sellIn);
        assertEquals(18, app.items[4].quality);
    }

    @Test
    @DisplayName("SellInValue should decrease for Aged Brie and BackStage Passes")
    void decreasesSellInValues()
    {
        Item[] items = new Item[]{
            new ItemBuilder("Aged Brie").toBeSoldIn(10).ofQuality(5).build(),
            new ItemBuilder("Backstage passes to a TAFKAL80ETC concert").toBeSoldIn(15).ofQuality(5).build()
        };

        GildedRose app = new GildedRose(items);

        app.updateQuality();
        assertEquals(9, app.items[0].sellIn);
        assertEquals(14, app.items[1].sellIn);
    }

    @Test
    @DisplayName("The Quality should lower twice as fast when the SellIn value becomes negative - Normal item case")
    void lowersQualityTwiceAsFastWhenSellInPassedForNormalItem()
    {
        Item[] items = new Item[]{new ItemBuilder("Normal Item").toBeSoldIn(1).ofQuality(10).build()};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(0, app.items[0].sellIn);
        assertEquals(9, app.items[0].quality);

        app.updateQuality();

        assertEquals(-1, app.items[0].sellIn);
        assertEquals(7, app.items[0].quality);

        app.updateQuality();

        assertEquals(-2, app.items[0].sellIn);
        assertEquals(5, app.items[0].quality);
    }

    @Test
    @DisplayName("The Quality should increase twice as fast when the SellIn value becomes negative - Aged Brie item case")
    void IncreasesQualityTwiceAsFastWhenSellInPassedForAgedBrie()
    {
        Item[] items = new Item[]{new ItemBuilder("Aged Brie").toBeSoldIn(1).ofQuality(10).build()};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(0, app.items[0].sellIn);
        assertEquals(11, app.items[0].quality);

        app.updateQuality();

        assertEquals(-1, app.items[0].sellIn);
        assertEquals(13, app.items[0].quality);

        app.updateQuality();

        assertEquals(-2, app.items[0].sellIn);
        assertEquals(15, app.items[0].quality);
    }

    @Test
    @DisplayName("The Quality should never be negative")
    void qualityNeverNegative()
    {
        Item[] items = new Item[]{new ItemBuilder("Normal Item").toBeSoldIn(10).ofQuality(1).build()};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(0, app.items[0].quality);

        app.updateQuality();

        assertEquals(0, app.items[0].quality);
    }

    @Test
    @DisplayName("The Quality should never go over 50 - Aged Brie case")
    void qualityNeverOver50AgedBrie()
    {
        Item[] items = new Item[]{new ItemBuilder("Aged Brie").toBeSoldIn(10).ofQuality(49).build()};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(50, app.items[0].quality);

        app.updateQuality();

        assertEquals(50, app.items[0].quality);
    }

    @Test
    @DisplayName("The Quality should never go over 50 - Back Stage Passes case")
    void qualityNeverOver50BackStagePasses()
    {
        Item[] items = new Item[]{new ItemBuilder("Backstage passes to a TAFKAL80ETC concert").toBeSoldIn(3).ofQuality(49).build()};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(50, app.items[0].quality);

        app.updateQuality();

        assertEquals(50, app.items[0].quality);
    }

    @Test
    @DisplayName("The Aged Brie item should have its Quality value increased with time")
    void agedBrieIncreasesQuality()
    {
        Item[] items = new Item[]{new ItemBuilder("Aged Brie").toBeSoldIn(10).ofQuality(1).build()};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(2, app.items[0].quality);

        app.updateQuality();

        assertEquals(3, app.items[0].quality);
    }

    @Test
    @DisplayName("The Sulfuras item should never be sold")
    void sulfurasNotToSell()
    {
        Item[] items = new Item[]{new ItemBuilder("Sulfuras, Hand of Ragnaros").toBeSoldIn(10).ofQuality(80).build()};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(10, app.items[0].sellIn);

        app.updateQuality();

        assertEquals(10, app.items[0].sellIn);
    }

    @Test
    @DisplayName("The Sulfuras should always keep the same quality value with time")
    void SulfurasAlwaysSameQuality()
    {
        Item[] items = new Item[]{new ItemBuilder("Sulfuras, Hand of Ragnaros").toBeSoldIn(10).ofQuality(80).build()};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(80, app.items[0].quality);

        app.updateQuality();

        assertEquals(80, app.items[0].quality);
    }

    @Test
    @DisplayName("The BackStage passes should have its Quality value increased by 1 when more than 10 days left")
    void backStagePassesShouldDecreaseBy1WhenMoreThan10DaysLeft()
    {
        Item[] items = new Item[]{new ItemBuilder("Backstage passes to a TAFKAL80ETC concert").toBeSoldIn(12).ofQuality(20).build()};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(11, app.items[0].sellIn);
        assertEquals(21, app.items[0].quality);

        app.updateQuality();

        assertEquals(10, app.items[0].sellIn);
        assertEquals(22, app.items[0].quality);
    }

    @Test
    @DisplayName("The BackStage passes should have its Quality value increased by 2 when between 5 and 10 days left")
    void backStagePassesShouldDecreaseBy2WhenBetween5And10DaysLeft()
    {
        Item[] items = new Item[]{new ItemBuilder("Backstage passes to a TAFKAL80ETC concert").toBeSoldIn(10).ofQuality(20).build()};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(9, app.items[0].sellIn);
        assertEquals(22, app.items[0].quality);

        app.updateQuality();

        assertEquals(8, app.items[0].sellIn);
        assertEquals(24, app.items[0].quality);

        app.updateQuality();

        assertEquals(7, app.items[0].sellIn);
        assertEquals(26, app.items[0].quality);

        app.updateQuality();

        assertEquals(6, app.items[0].sellIn);
        assertEquals(28, app.items[0].quality);

        app.updateQuality();

        assertEquals(5, app.items[0].sellIn);
        assertEquals(30, app.items[0].quality);
    }

    @Test
    @DisplayName("The BackStage passes should have its Quality value increased with time by 3 when sellIn is 5 days or less")
    void backStagePassesShouldDecreaseBy3When5DaysOrLessLeft()
    {
        Item[] items = new Item[]{new ItemBuilder("Backstage passes to a TAFKAL80ETC concert").toBeSoldIn(5).ofQuality(20).build()};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(4, app.items[0].sellIn);
        assertEquals(23, app.items[0].quality);

        app.updateQuality();

        assertEquals(3, app.items[0].sellIn);
        assertEquals(26, app.items[0].quality);

        app.updateQuality();

        assertEquals(2, app.items[0].sellIn);
        assertEquals(29, app.items[0].quality);

        app.updateQuality();

        assertEquals(1, app.items[0].sellIn);
        assertEquals(32, app.items[0].quality);

        app.updateQuality();

        assertEquals(0, app.items[0].sellIn);
        assertEquals(35, app.items[0].quality);
    }

    @Test
    @DisplayName("The BackStage passes should have its Quality to 0 when the SellIn is negative")
    void backStagePassesQualityAfterNegativeSellIn()
    {
        Item[] items = new Item[]{new ItemBuilder("Backstage passes to a TAFKAL80ETC concert").toBeSoldIn(1).ofQuality(20).build()};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(0, app.items[0].sellIn);
        assertEquals(23, app.items[0].quality);

        app.updateQuality();

        assertEquals(-1, app.items[0].sellIn);
        assertEquals(0, app.items[0].quality);

        app.updateQuality();

        assertEquals(-2, app.items[0].sellIn);
        assertEquals(0, app.items[0].quality);
    }

    @Test
    @DisplayName("The Conjured Item should have its Quality decrease twice as fast")
    void conjuredItemDecreasesTwiceAsFast()
    {
        Item[] items = new Item[]{new ItemBuilder("Conjured Mana Cake").toBeSoldIn(10).ofQuality(20).build()};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(9, app.items[0].sellIn);
        assertEquals(18, app.items[0].quality);

        app.updateQuality();

        assertEquals(8, app.items[0].sellIn);
        assertEquals(16, app.items[0].quality);

        app.updateQuality();

        assertEquals(7, app.items[0].sellIn);
        assertEquals(14, app.items[0].quality);
    }

    @Test
    @DisplayName("The Quality of the conjured Item should never be negative")
    void qualityNeverNegativeConjuredItem()
    {
        Item[] items = new Item[]{new ItemBuilder("Conjured Mana Cake").toBeSoldIn(10).ofQuality(2).build()};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertEquals(0, app.items[0].quality);

        app.updateQuality();

        assertEquals(0, app.items[0].quality);
    }

}
