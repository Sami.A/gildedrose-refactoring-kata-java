package com.gildedrose;

import com.gildedrose.updater.provider.UpdaterProvider;

class GildedRose
{
    Item[] items;

    public GildedRose(Item[] items)
    {
        this.items = items;
    }


    /**
     * Updates all the Items' Quality and SellIn values
     */
    public void updateQuality()
    {
        for (Item item : items) {
            updateItem(item);
        }
    }

    /**
     * Updates the Quality and sellIn of the given Item according to its type.
     *
     * @param item the item to update
     */
    private void updateItem(Item item)
    {
        UpdaterProvider.getUpdaterFor(item.name).update(item);
    }
}
