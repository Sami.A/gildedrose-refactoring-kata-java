package com.gildedrose.updater.provider;

import com.gildedrose.updater.*;
import com.gildedrose.updater.contract.ItemUpdater;

import java.util.HashMap;
import java.util.Map;


public class UpdaterProvider
{
    static final String SULFURAS_NAME = "Sulfuras, Hand of Ragnaros";
    static final String AGED_BRIE_NAME = "Aged Brie";
    static final String BACKSTAGE_PASSES_NAME = "Backstage passes to a TAFKAL80ETC concert";
    static final String CONJURED_NAME = "Conjured Mana Cake";


    static Map<String, ItemUpdater> itemTypes = new HashMap<>();

    static final StandardUpdater standardUpdater = new StandardUpdater();

    static {
        itemTypes.put(SULFURAS_NAME, new LegendaryUpdater());
        itemTypes.put(AGED_BRIE_NAME, new ImprovesQualityUpdater());
        itemTypes.put(BACKSTAGE_PASSES_NAME, new BackStagePassesUpdater());
        itemTypes.put(CONJURED_NAME, new ConjuredUpdater());
    }

    /**
     * Based on the name of the item, returns the correct updater
     *
     * @param name name of the item you want to update
     * @return the correct updater for the item
     */
    public static ItemUpdater getUpdaterFor(String name)
    {
        return itemTypes.getOrDefault(name, standardUpdater);
    }
}
