package com.gildedrose.updater;

import com.gildedrose.Item;
import com.gildedrose.updater.contract.ItemUpdater;

public class LegendaryUpdater extends ItemUpdater
{

    /**
     * Legendary items cannot be sold and their quality never varies.
     *
     * @param item item to update
     */
    @Override
    public void update(Item item)
    {
        //The legendary item should not be updated, its quality and sellIn values should remain the same
    }
}
