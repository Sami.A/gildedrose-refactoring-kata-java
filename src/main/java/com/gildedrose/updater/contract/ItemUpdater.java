package com.gildedrose.updater.contract;

import com.gildedrose.Item;

public abstract class ItemUpdater
{
    protected static final int MIN_QUALITY = 0;
    protected static final int MAX_QUALITY = 50;

    /**
     * Here should be defined the update Quality behaviour of the Item
     *
     * @param item item to update
     */
    public abstract void update(Item item);

    /**
     * Increases the quality of an item by 1 with a maximum of 50
     *
     * @param item item of which you want to increase the quality
     */
    protected void increaseQuality(Item item)
    {
        if (item.quality < MAX_QUALITY) {
            item.quality++;
        }
    }

    /**
     * Decreases the quality of an item by 1 with a minimum of 0
     *
     * @param item item of which you want to decrease the quality
     */
    protected void decreaseQuality(Item item)
    {
        if (item.quality > MIN_QUALITY) {
            item.quality--;
        }
    }

    /**
     * Decreases the item SellIn
     *
     * @param item item to age
     */
    protected void ageItem(Item item)
    {
        item.sellIn--;
    }
}
