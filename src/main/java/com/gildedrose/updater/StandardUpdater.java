package com.gildedrose.updater;

import com.gildedrose.Item;
import com.gildedrose.updater.contract.ItemUpdater;

public class StandardUpdater extends ItemUpdater
{

    /**
     * This is the standard updater, items will have their quality decrease with time.
     * Once the sell by date passes, they decrease twice as fast.
     *
     * @param item item to update
     */
    @Override
    public void update(Item item)
    {
        decreaseQuality(item);

        //if sellByDate passed, quality decreases twice as fast
        if (item.sellIn <= 0)
            decreaseQuality(item);

        ageItem(item);
    }
}
