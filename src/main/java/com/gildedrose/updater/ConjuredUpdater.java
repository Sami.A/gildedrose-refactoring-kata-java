package com.gildedrose.updater;

import com.gildedrose.Item;
import com.gildedrose.updater.contract.ItemUpdater;

public class ConjuredUpdater extends ItemUpdater
{
    /**
     * This updater is for the items that have their quality decrease over time twice as fast as the standard items.
     *
     * @param item item to update
     */
    @Override
    public void update(Item item)
    {
        //quality decreases twice as fast
        decreaseQuality(item);
        decreaseQuality(item);
        ageItem(item);
    }
}
