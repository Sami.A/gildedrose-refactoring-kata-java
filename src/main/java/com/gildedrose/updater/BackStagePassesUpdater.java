package com.gildedrose.updater;

import com.gildedrose.Item;
import com.gildedrose.updater.contract.ItemUpdater;

public class BackStagePassesUpdater extends ItemUpdater
{
    /**
     * This updater is for the Backstage passes. Increases in Quality as its SellIn value approaches;
     * Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but
     * Quality drops to 0 after the concert
     *
     * @param item item to update
     */
    @Override
    public void update(Item item)
    {
        //if sellByDate passed, quality is always 0
        if (item.sellIn <= 0) {
            item.quality = MIN_QUALITY;
        }
        else {
            // more than 10 days left, quality increases normally
            increaseQuality(item);

            if (item.sellIn < 11) // between 10 to 5 days left, quality increases by 2
            {
                increaseQuality(item);
            }
            if (item.sellIn < 6) { // last 5 days, quality increases by 3
                increaseQuality(item);
            }
        }

        ageItem(item);
    }
}
