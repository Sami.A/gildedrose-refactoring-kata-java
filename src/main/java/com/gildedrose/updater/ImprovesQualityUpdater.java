package com.gildedrose.updater;

import com.gildedrose.Item;
import com.gildedrose.updater.contract.ItemUpdater;

public class ImprovesQualityUpdater extends ItemUpdater
{
    /**
     * This updater is for the items that have their quality increase over time.
     * When the sellIn gets to 0 or under, that means the sell by date passed.
     * This is resulting in the item having it's quality increased twice as fast.
     *
     * @param item item to update
     */
    @Override
    public void update(Item item)
    {
        increaseQuality(item);

        //if sellByDate passed, quality increases twice as fast
        if (item.sellIn <= 0)
            increaseQuality(item);

        ageItem(item);
    }
}
