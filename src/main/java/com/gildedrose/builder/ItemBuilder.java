package com.gildedrose.builder;

import com.gildedrose.Item;

/**
 * Allows you to easily build an Item while improving readability
 */
public class ItemBuilder
{
    private String name;
    private int sellIn;
    private int quality;

    private ItemBuilder()
    {
        //making no args constructor private so we have to build an item starting with a name
    }

    /**
     * Constructor to start building an item with the given name and default values for the other fields.
     * You can call the other methods to set the values of the other fields.
     * When done building the Item, call the {@link #build()} method to get the built Item.
     *
     * @param name name of the Item to build
     */
    public ItemBuilder(String name)
    {
        this.name = name;
        this.sellIn = 0;
        this.quality = 0;
    }

    /**
     * Allows you to set a SellIn value for the item
     *
     * @param sellIn sellIn value to be set in the item
     * @return the itemBuilder to continue building
     */
    public ItemBuilder toBeSoldIn(int sellIn)
    {
        this.sellIn = sellIn;

        return this;
    }

    /**
     * Allows you to set a Quality value for the item
     *
     * @param quality quality value to be set in the item
     * @return the itemBuilder to continue building
     */
    public ItemBuilder ofQuality(int quality)
    {
        this.quality = quality;

        return this;
    }

    /**
     * Builds the Item with the previously set values and returns it
     *
     * @return the Item built
     */
    public Item build()
    {
        return new Item(name, sellIn, quality);
    }

}
